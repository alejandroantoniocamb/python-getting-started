# python-getting-started

Esta es una aplicacion de Python basica, el staging se encuentra desplegado [en este link](https://staging-qap3.herokuapp.com/) y el ambiente de produccion se depliega en [este](https://production-qap3.herokuapp.com/).

Esta aplicacion soporta el articulo oficial de heroku [Getting Started with Python on Heroku](https://devcenter.heroku.com/articles/getting-started-with-python) article - check it out.

## Corriendo el proyecto localmente

Asegurate de tener python instalado: [tutorial en este link](http://install.python-guide.org).  Tambien, instala el [Heroku Toolbelt](https://toolbelt.heroku.com/) y [Postgres](https://devcenter.heroku.com/articles/heroku-postgresql#local-setup).

para utilizar foreman desde mac tuve que instalarlo utilizando ruby. 

```sh
$ git clone git@github.com:heroku/python-getting-started.git
$ cd python-getting-started
$ pip install -r requirements.txt
$ createdb python_getting_started
$ foreman run python manage.py migrate
$ python manage.py collectstatic
$ foreman start web
```

Tu aplicacion deberia correr en [localhost:5000](http://localhost:5000/).


## Corriendo el proyecto localmente (docker)

desde el directorio del proyecto, ejecuta:

```sh
$ docker-compose up
```

en caso de que necesites volver a cargar el container de docker:

```sh
$ docker-compose up --build
```

probado y funcionando con la version de docker: 

```
Docker version 18.09.2, build 6247962
```


Tu aplicacion deberia correr en [0.0.0.0:3000](http://0.0.0.0:3000/).
En Windows corre en la direccion del servidor montado por la maquina virtual, generalmente 192.168.99.100

## Notas algunas (probado en Windows)
Para iniciar los contenedores:
  docker-compose start
Para correr migraciones (Grax Jesús):
  docker exec -it id del contenedor del proyecto python manage.py migrate
Para ejecutar las pruebas unitarias:
  docker exec -it id del contenedor del proyecto python manage.py test hello
Para correr la app:
  docker-compose up
Para ir al módulo de tutores:
  http://192.168.99.100:3000/tgs/crearTutor

## Despliegue a Heroku (no aplica para desarrolladores ni BBDD)

```sh
$ heroku create
$ git push heroku master
$ heroku open
```

## Documentacion

Para mas informacion sobre usar python con heroku, ver los articulos de dev center de heroku:

- [Python on Heroku](https://devcenter.heroku.com/categories/python)
