FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR /gettingstarted
COPY requirements.txt /gettingstarted/
RUN pip install -r requirements.txt
COPY . /gettingstarted/