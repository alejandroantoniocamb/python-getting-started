# coding=utf-8
from __future__ import unicode_literals

from django.db import models
from django.core import validators

# Create your models here.
class Greeting(models.Model):
    when = models.DateTimeField('date created', auto_now_add=True)

class AreaConocimiento(models.Model):
    nombre = models.CharField(max_length=255)
    definicion = models.CharField(max_length=255)
    area_padre = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        db_table = 'area_conocimiento'


class Autor(models.Model):
    cedula = models.IntegerField(unique=True)
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)

    class Meta:
        db_table = 'autor'


class AutorTg(models.Model):
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE)
    tg = models.ForeignKey('Tg', on_delete=models.CASCADE)
    
    class Meta:
        db_table = 'autor_tg'
        unique_together = (('autor', 'tg'),)


class Evento(models.Model):
    nombre = models.CharField(max_length=255)
    definicion = models.CharField(max_length=255)
    area_conocimiento = models.ForeignKey(AreaConocimiento, on_delete=models.CASCADE)

    class Meta:
        db_table = 'evento'


class Mencion(models.Model):
    nombre = models.CharField(max_length=255)

    class Meta:
        db_table = 'mencion'


class MencionTitulo(models.Model):
    titulo = models.ForeignKey('Titulo', on_delete=models.CASCADE)
    mencion = models.ForeignKey(Mencion, on_delete=models.CASCADE)

    class Meta:
        db_table = 'mencion_titulo'
        unique_together = (('titulo', 'mencion'),)

# Equipo Gestión de TG - Inicio
class Tg(models.Model):
    titulo = models.CharField(max_length=255, validators=[validators.MinLengthValidator(10, "El título es muy corto")])
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    tipo_tg = models.ForeignKey('TipoTg', on_delete=models.CASCADE)
    # mencion_titulo = models.ForeignKey(MencionTitulo, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        db_table = 'tg'


class TipoTg(models.Model):
    nombre = models.CharField(max_length=255, validators=[validators.MinLengthValidator(3, "El nombre del tipo de TG es muy corto")])

    class Meta:
        db_table = 'tipo_tg'


class TipoTutor(models.Model):
    nombre = models.CharField(max_length=255)
    def __str__(self):
        return str(self.nombre)
    class Meta:
        db_table = 'tipo_tutor'


class Titulo(models.Model):
    nombre = models.CharField(max_length=255)

    class Meta:
        db_table = 'titulo'


class Tutor(models.Model):
    cedula = models.IntegerField(unique=True)
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    tipo_tutor = models.ForeignKey(TipoTutor, on_delete=models.CASCADE)

    class Meta:
        db_table = 'tutor'
