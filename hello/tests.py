# import unittest
from django.test import TestCase
from django.test import Client
from hello.models import Tg, TipoTg, Tutor, TipoTutor
import datetime


# Create your tests here.
class CrearTg(TestCase):
    def test_tg_valido(self):
        tt = TipoTg(nombre='TIG')
        tt.save()

        tg = Tg(
            titulo='Un Trabajo de Grado Grandioso',
            fecha_inicio='2019-10-10',
            fecha_fin='2019-12-10',
            tipo_tg_id=1
        )

        print('TipoTgs: ', TipoTg.objects.count())

        tg.save()

        try:
            tg = Tg.objects.first()
        except Tg.DoesNotExist:
            tg = None

        self.assertIsNotNone(tg)

    def test_titulo_debe_ser_mas_largo(self):
        tt = TipoTg(nombre='TIG')
        tt.save()

        tg = Tg(
            titulo='a' * 9,
            fecha_inicio='2019-10-10',
            fecha_fin='2019-12-10',
            tipo_tg=TipoTg.objects.first()
        )

        try:
            tg.full_clean()
            tg.save()
        except Exception as e:
            print('Mensaje: ' + str(e))

        try:
            tg = Tg.objects.first()
        except Tg.DoesNotExist:
            tg = None

        self.assertIsNone(tg)

class CrearTipoTg(TestCase):
  def test_titulo_debe_ser_mas_largo(self):
    ttg = TipoTg(nombre='TG')

    try:
        ttg.full_clean()
        ttg.save()
    except Exception as e:
        print('Mensaje: ' + str(e))

    try:
        ttg = TipoTg.objects.first()
    except TipoTg.DoesNotExist:
        ttg = None

    self.assertIsNone(ttg)

class Test_Tutor(TestCase):
  def test_crear_nuevo_tutor(self):
    tt = TipoTutor(nombre = 'Académico')
    tt.save()
    tu = Tutor(
      cedula=123456,
      nombre='Pedro',
      apellido='Pérez',
      tipo_tutor=tt)
    tu.save()
    try:
      tu = Tutor.objects.first()
    except Tutor.DoesNotExist:
      tu = None
    self.assertIsNotNone(tu)
  def test_listar_tutores(self):
      tipoTutor = TipoTutor(nombre='Académico')
      tipoTutor.save()
      tutor1 = Tutor(
          cedula=123456,
          nombre='Pedro',
          apellido='Pérez',
          tipo_tutor=tipoTutor)
      tutor2 = Tutor(
          cedula=38472,
          nombre='Maria',
          apellido='Dominguez',
          tipo_tutor=tipoTutor)
      tutor1.save()
      tutor2.save()
      tutores = Tutor.objects.all()
      self.assertEqual(tutores.__len__(), 2)
  def test_modificar_tutor(self):
      tt = TipoTutor(nombre='Académico')
      tt.save()
      tu = Tutor(
          cedula=123456,
          nombre='Pedro',
          apellido='Pérez',
          tipo_tutor=tt)
      tu.save()
      Tutor.objects.filter(id=1).update(nombre='Francisco')
      self.assertIsNotNone(Tutor.objects.filter(nombre='Francisco'))
  def test_filtrar_tipo_tutor(self):
      # Se crean y se guardan los tipos de tutor
      tipoTutor1 = TipoTutor(nombre='Interno')
      tipoTutor2 = TipoTutor(nombre='Externo')
      tipoTutor1.save()
      tipoTutor2.save()
      # Se crean y se guardan los tutores
      id_tipoTutor1 = TipoTutor.objects.filter(nombre='Interno')[0].id
      id_tipoTutor2 = TipoTutor.objects.filter(nombre='Externo')[0].id
      tutor1 = Tutor(cedula=1234567, nombre='Pedro', apellido='Pérez', tipo_tutor_id=id_tipoTutor1)
      tutor2 = Tutor(cedula=3847278, nombre='Maria', apellido='Dominguez', tipo_tutor_id=id_tipoTutor2)
      tutor1.save()
      tutor2.save()
      # Se busca el tutor de acuerdo a su tipo
      # Como se sabe que el tutor2 es el externo, contra ese se compara el tutor buscado
      tipoBuscado = TipoTutor.objects.filter(nombre='Externo')[0]
      tutorBuscado = Tutor.objects.filter(tipo_tutor_id=tipoBuscado.id)[0]
      self.assertEqual(tutorBuscado.id, tutor2.id)