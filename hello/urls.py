from django.conf.urls import include, url
from . import views

urlpatterns = [
  url(r'^$', views.tg_index, name='tg_index'),
  url(r'^new', views.new_tg, name='new_tg'),
  url(r'^<int:tg_id>', views.show_tg, name='show_tg'),
  url(r'^crearTipo', views.crearTipo, name='crearTipo'),
  url(r'^tiposTutor', views.tiposTutor, name='tiposTutor'),
  url(r'^crearTutor', views.crearTutor, name='crearTutor'),
  url(r'^tutores', views.tutores, name='tutores'),
]
