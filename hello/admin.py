from django.contrib import admin
from hello.models import Tg, TipoTg, TipoTutor, Tutor

# Register your models here.
admin.site.register(Tg)
admin.site.register(TipoTg)
admin.site.register(TipoTutor)
admin.site.register(Tutor)
