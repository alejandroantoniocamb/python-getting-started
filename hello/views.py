from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.contrib import messages
from django.http import HttpResponseRedirect
from .models import Greeting, Tg, TipoTg, TipoTutor, Tutor
# from . import forms
from hello.forms import NewTgForm
#from hello.models import Tg, TipoTg

# Create your views here.
def index(request):
    # return HttpResponse('Hello from Python!')
    return render(request, 'index.html')


def db(request):
    greeting = Greeting()
    greeting.save()
    greetings = Greeting.objects.all()
    return render(request, 'db.html', {'greetings': greetings})

def tg_index(request):
  tgs = Tg.objects.all()
  context = { 'tgs': tgs }
  return render(request, 'tgs/index.html', context)

def show_tg(request, tg_id):
  tg = get_object_or_404(Tg, pk=tg_id)
  return render(request, 'tgs/show.html', { 'tg': tg })

def new_tg(request):
  form = NewTgForm()

  if request.method == 'POST':
    form = NewTgForm(request.POST)

    if form.is_valid():
      form.save(commit=True)
      return tg_index(request)
    else:
      print('El formulario es inválido')

  return render(request, 'tgs/new.html', { 'form': form })

def crearTipo(request):
    try:
        nombre = request.POST['nombre']
        
    except (KeyError, TipoTutor.DoesNotExist):
      return render(request, 'tgs/crearTipo.html', {
        'error_message': "No ingreso datos"
        })
    else:
      tipo = TipoTutor(nombre=nombre)
      tipo.save()
    return render(request, 'tgs/tiposTutor.html', {'tipos': TipoTutor.objects.all()})

def tiposTutor(request):
  return render(request, 'tgs/tiposTutor.html', {'tipos': TipoTutor.objects.all()})

def crearTutor(request):
    try:
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        cedula = request.POST['cedula']
        tipo = request.POST['tipo']
    except (KeyError, Tutor.DoesNotExist):
      return render(request, 'tgs/crearTutor.html', {
        'tipo_tutores': TipoTutor.objects.all()
        })
    else:
      tutor = Tutor(nombre=nombre, apellido=apellido, cedula=cedula, tipo_tutor_id=tipo)
      tutor.save()
    return render(request, 'tgs/tutores.html', {'tutores': Tutor.objects.all()})

def tutores(request):
  return render(request, 'tgs/tutores.html', {'tutores': Tutor.objects.all()})
