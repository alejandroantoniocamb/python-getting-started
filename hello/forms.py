from django import forms
from hello.models import Tg

class NewTgForm(forms.ModelForm):
  class Meta:
    model = Tg
    fields = '__all__'
